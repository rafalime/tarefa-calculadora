/* Funcionalidade 1: Adicionar nota à lista de notas
    1) Criar função que coleta nota do input e adiciona a lista quando clicado o botão de adicionar
    --> DOM: Criar parágrafo com a nota
    --> Back: Converter nota e adiciona-la em um array que conterá todas as notas 


*/

/* Funcinalidade: Coletar dados do input e adicioná-los a lista quando o botão adicionar é clicado */
let grades = []
let input = document.querySelector(".inputGrade")
let inputbtn = document.querySelector(".add")
let calculatebtn = document.querySelector(".calculate")
const list = document.querySelector(".grades")
const output = document.querySelector(".output")
let contador = 1

function addGrade(){
    //Pegar o valor do campo input e verificar se o valor é um número
    var grade = parseFloat(input.value)
    if (isNaN(grade)){
        alert("A nota digitada é inválida, por favor, insira uma nota válida")
        input.value = ''
    }else{
        // Adicionar filtro de notas válidas entre 0 e 10
        if ((grade > 10) || (grade < 0)){
            alert("A nota digitada é inválida, por favor, insira uma nota válida")
            input.value = ''
        }else{
            grades.push(grade)
            // Criar um elemento <p> que terá o valor como texto dentro
            let value = document.createElement("p")
            value.innerText = `A nota ${contador} foi ${grade}`
            // Incrementar contador
            contador++
            list.append(value)
            input.value = ''
        }
        
    }
}
// Adicionar um event listener para essa função no botão
inputbtn.addEventListener("click", addGrade)

/* Funcionalidade 2: Pegar todos os elementos da lista, e calcular a média deles, imprimir resultado na tela.*/

function getmedia(){
    //Calcular a soma do array e dividir pelo número de elementos (lenght do array)
    if (grades.length == 0){
        // Se o array estiver vazio, informar um erro
        alert("Por favor, insira uma nota")
    }else{
        // Cálculo da média
        let media = 0
        for (num of grades){
            media = media + num
        }
        media = media/grades.length
        // console.log(media)
        // Imprimir o resultado no output da página
        // Corrigir número para arredondar para 2 casas decimais
        media = media.toFixed(2)
        output.innerText = `A média é: ${media}`
    }   
}
// Adicionar event listener para o botão "Calcular Média"
calculatebtn.addEventListener("click", getmedia)